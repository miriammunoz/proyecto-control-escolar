﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;
using System.IO;
using MySql.Data.MySqlClient;

namespace ControlEscolar
{
    public partial class frmEscuela : Form
    {
        private EscuelaManejador _EscuelaManejador;
        private OpenFileDialog _imagen;
        private string _ruta;
        public frmEscuela()
        {
            InitializeComponent();
            _EscuelaManejador = new EscuelaManejador();
            _imagen = new OpenFileDialog();
            _ruta = Application.StartupPath + "\\Logo\\";
        }
        private void frmEscuela_Load(object sender, EventArgs e)
        {
            // BuscarEscuela("");
            ControlarBotones(true, false, false, true, false, false);
            ControlarCuadros(false);
            // Registros();
        }
        private void Registros()
        {
            DataSet rg = _EscuelaManejador.Registro();
            lblid.Text = rg.Tables[0].Rows[0]["idescuela"].ToString();
            txtNombre.Text = rg.Tables[0].Rows[0]["Nombre"].ToString();
            txtRFC.Text = rg.Tables[0].Rows[0]["RFC"].ToString();
            txtDomicilio.Text = rg.Tables[0].Rows[0]["Domicilio"].ToString();
            txtTelefono.Text = rg.Tables[0].Rows[0]["Telefono"].ToString();
            txtPaginaWeb.Text = rg.Tables[0].Rows[0]["PaginaWeb"].ToString();
            txtCorreo.Text = rg.Tables[0].Rows[0]["Correo"].ToString();
            txtDirector.Text = rg.Tables[0].Rows[0]["Director"].ToString();
            txtRuta.Text = rg.Tables[0].Rows[0]["Logo"].ToString();

            if (txtRuta.Text != "")
            {
                Logo.ImageLocation = @"C:\Users\Miria\source\repos\ControlEscolar\ControlEscolar\bin\Debug\Logo\" + txtRuta.Text;
            }

        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar, bool Importar, bool Quitar)
        {
            btnVer.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnImportar.Enabled = Importar;
            btnQuitar.Enabled = Quitar;
        }
        private void ControlarCuadros(bool activar)
        {

            txtNombre.Enabled = activar;
            txtRFC.Enabled = activar;
            txtDomicilio.Enabled = activar;
            txtTelefono.Enabled = activar;
            txtPaginaWeb.Enabled = activar;
            txtCorreo.Enabled = activar;
            txtDirector.Enabled = activar;
            txtRuta.Enabled = activar;
        }
        /*private void BuscarEscuela(string filtro)
        {
            dgvEscuela.DataSource=_EscuelaManejador.GetEscuela(filtro);
        }*/

        private void GuardarEscuela()
        {
                _EscuelaManejador.Guardar(new Escuela
                {
                    Id = Convert.ToInt32(lblid.Text),
                    Nombre = txtNombre.Text,
                    Rfc = txtRFC.Text,
                    Domicilio = txtDomicilio.Text,
                    Telefono = txtTelefono.Text,
                    Paginaweb = txtPaginaWeb.Text,
                    Correo = txtCorreo.Text,
                    Director = txtDirector.Text,
                    Logo = txtRuta.Text,

                });
            
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true, false, false);
            ControlarCuadros(false);
           
            if(Logo.Image==null)
            {
                EliminarDocumento();
            }
            else
            {
                try
                {
                    GuardarEscuela();
                    //BuscarEscuela("");
                    //HacerCarpeta();
                    GuardarDocumento();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
           ControlarBotones(true, false, false, true, false, false);
            ControlarCuadros(false);
            Registros();
        }
        /* private void ModificarEscuela()
         {
             ControlarBotones(false, true, true, false, true, true);
             ControlarCuadros(true);

            string r = @"C:\Users\Miria\source\repos\ControlEscolar\ControlEscolar\bin\Debug\Logo\" + txtNombre.Text + @"\";

           lblid.Text = dgvEscuela.CurrentRow.Cells["id"].Value.ToString();
           txtNombre.Text = dgvEscuela.CurrentRow.Cells["Nombre"].Value.ToString();
           txtRFC.Text = dgvEscuela.CurrentRow.Cells["RFC"].Value.ToString();
           txtDomicilio.Text = dgvEscuela.CurrentRow.Cells["Domicilio"].Value.ToString();
           txtTelefono.Text = dgvEscuela.CurrentRow.Cells["Telefono"].Value.ToString();
           txtPaginaWeb.Text = dgvEscuela.CurrentRow.Cells["PaginaWeb"].Value.ToString();
           txtCorreo.Text=dgvEscuela.CurrentRow.Cells["Correo"].Value.ToString();
           txtDirector.Text = dgvEscuela.CurrentRow.Cells["Director"].Value.ToString();
           txtRuta.Text = dgvEscuela.CurrentRow.Cells["Logo"].Value.ToString();
           Logo.ImageLocation = _ruta +"\\"+ dgvEscuela.CurrentRow.Cells["Logo"].Value.ToString();
         }*/

        private void btnImportar_Click(object sender, EventArgs e)
        {
            CargarDocumento();
            this.timer1.Start();
        }
        private void CargarDocumento()
        {
            _imagen.Filter = "Imagen tipo (*.jpg)|*.jpg";
            _imagen.Title = "Cargar Imagen";
            _imagen.ShowDialog();

            if (_imagen.FileName != "")
            {
                var archivo = _imagen.FileName;
                Bitmap imagen = new Bitmap(archivo);

                var archivos = new FileInfo(_imagen.FileName);
                if (archivos.Length < 3000000)
                {
                    Logo.Image = imagen;
                    txtRuta.Text = archivos.Name;
                }
                else
                {
                    MessageBox.Show("No se puede cargar imagen mayor a 3MB");
                }
            }
        }

        private void GuardarDocumento()
        {

            if (_imagen.FileName != null)
            {
                if (_imagen.FileName != "")
                {
                    var archivo = new FileInfo(_imagen.FileName);

                    if (Directory.Exists(_ruta))
                    {
                        //Codigo para agregar archivo
                        var ObternerArchivos = Directory.GetFiles(_ruta, "*.jpg");

                        FileInfo archivoAnterior;

                        if (ObternerArchivos.Length != 0)
                        {
                            //Codigo para remplazar imagen
                            archivoAnterior = new FileInfo(ObternerArchivos[0]);

                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();
                                archivo.CopyTo(_ruta + archivo.Name);
                            }
                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name);
                    }
                }
            }
        }


    
      private void EliminarDocumento()
        {
            if (MessageBox.Show("Estas seguro que deseas elimanar la imagen", "Eliminar Imagen", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (Directory.Exists(_ruta))
                {
                    var ObternerArchivos = Directory.GetFiles(_ruta, "*.jpg");

                    FileInfo archivoAnterior;

                    if (ObternerArchivos.Length != 0)
                    {
                        archivoAnterior = new FileInfo(ObternerArchivos[0]);

                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();
                            txtRuta.Text = "";
                            Logo.ImageLocation = null;
                        }
                    }
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.pbImagen.Increment(5);
        }

        private void btnQuitar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro que deseas eliminar esta imagen?", "Eliminar Imagen", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    //EliminarDocumento();
                    Logo.Image = null;
                    txtRuta.Text = "";
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        
        private void btnVer_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false, true,true);
            ControlarCuadros(true);
            //BuscarEscuela("");
            Registros();
           // ModificarEscuela();
        }
    }
}
