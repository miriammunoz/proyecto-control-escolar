﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;
using System.IO;

namespace ControlEscolar
{
    public partial class frmEstudios : Form
    {
        private EstudiosManejador _EstudiosManejador;
        private MaestrosManejador _MaestrosManejador;
        private OpenFileDialog _documento;
        private string _ruta;
        public frmEstudios()
        {
            InitializeComponent();
            _EstudiosManejador = new EstudiosManejador();
            _MaestrosManejador = new MaestrosManejador();
            _documento = new OpenFileDialog();
            _ruta = Application.StartupPath + "\\Cedulas\\";
        }

        private void Estudios_Load(object sender, EventArgs e)
        {
            BuscarEstudios("");
            ControlarBotones(true, false, false, true,false);
            ControlarCuadros(false);
            LimpiarCuadros();
            TraerMaestro("");
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar, bool Importar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
            btnImportar.Enabled = Importar;
            
        }
        private void ControlarCuadros(bool activar)
        {

            txtcedula.Enabled = activar;
            txttitulo.Enabled = activar;
            txtRuta.Enabled = activar;
            cbxMaestro.Enabled = activar;
        }
        private void LimpiarCuadros()
        {

            txtcedula.Text = "";
            txttitulo.Text = "";
            lblId.Text = "0";
            txtRuta.Text = "";
        }
        private void BuscarEstudios(string filtro)
        {
            dgvEstudios.DataSource = _EstudiosManejador.GetEstudios(filtro);
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false,true);
            ControlarCuadros(true);
        }
        private void GuardarEstudios()
        {
            _EstudiosManejador.Guardar(new Estudios
            {
                Id = Convert.ToInt32(lblId.Text),
                Fkidmaestro = Convert.ToInt32(cbxMaestro.SelectedValue),
                Cedula = txtcedula.Text,
                Titulo = txttitulo.Text,
                Documento = txtRuta.Text,

            });
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true, false);
            ControlarCuadros(false);

            try
            {
                GuardarEstudios();
                LimpiarCuadros();
                BuscarEstudios("");
                HacerCarpeta();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true,false);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void EliminarEstudio()
        {
            var Id = dgvEstudios.CurrentRow.Cells["Id"].Value;
            _EstudiosManejador.Eliminar(Convert.ToInt32(Id));
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro que deseas eliminar este registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarEstudio();
                    BuscarEstudios("");
                    EliminarDocumento();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void ModificarEstudios()
        {
            ControlarBotones(false, true, true, false,true);
            ControlarCuadros(true);

            lblId.Text = dgvEstudios.CurrentRow.Cells["Id"].Value.ToString();
            cbxMaestro.Text = dgvEstudios.CurrentRow.Cells["idmaestro"].Value.ToString();
            txtcedula.Text = dgvEstudios.CurrentRow.Cells["Cedula"].Value.ToString();
            txttitulo.Text = dgvEstudios.CurrentRow.Cells["Titulo"].Value.ToString();
            txtRuta.Text = dgvEstudios.CurrentRow.Cells["Documento"].Value.ToString();

        }

        private void dgvEstudios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarEstudios();
                BuscarEstudios("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        /*private void txtNumeroControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("Solo se permiten Numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }*/

        private void btnclose_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarEstudios(txtBuscar.Text);
        }

        private void btnImportar_Click(object sender, EventArgs e)
        {
            CargarDocumento();
            this.timer1.Start();
        }
        private void CargarDocumento()
        {
            _documento.Filter = "Documento tipo (*.pdf)|*.pdf";
            _documento.Title = "Cargar Documento";
            _documento.ShowDialog();

            if (_documento.FileName != "")
            {
                var archivo = new FileInfo(_documento.FileName);
                txtRuta.Text = archivo.Name;
            }
        }
        private void HacerCarpeta()
        {
            string r = @"C:\Users\Miria\source\repos\ControlEscolar\ControlEscolar\bin\Debug\Cedulas\" + cbxMaestro.Text + @"\";
            if(System.IO.File.Exists(r))
            {
               
            }
            else
            {
                Directory.CreateDirectory(r);
                GuardarDocumento();
            }
        }
        private void GuardarDocumento()
        {

            string r = @"C:\Users\Miria\source\repos\ControlEscolar\ControlEscolar\bin\Debug\Cedulas\" + cbxMaestro.Text + @"\";
           // var archivo = new FileInfo(_documento.FileName);

            if (_documento.FileName != null)
            {
                if (_documento.FileName != "")
                {
                    var archivo = new FileInfo(_documento.FileName);
                    archivo.CopyTo(r +@"\"+ archivo.Name);
                    
                }
            }
        }

        private void EliminarDocumento()
        {
           // var archivo = new FileInfo(_documento.FileName);

            string r = @"C:\Users\Miria\source\repos\ControlEscolar\ControlEscolar\bin\Debug\Cedulas\" + cbxMaestro.Text + @"\" ;
            MessageBox.Show(r);
            if (System.IO.Directory.Exists(r))
            {
                System.IO.Directory.Delete(r,true);
                txtRuta.Text = "";
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.pbPDF.Increment(5);
        }

        private void TraerMaestro(string filtro)
        {
            cbxMaestro.DataSource = _MaestrosManejador.GetMaestros(filtro);
            cbxMaestro.DisplayMember = "Nombre";
            cbxMaestro.ValueMember = "idmaestro";
        }

        private void cbxMaestro_Click(object sender, EventArgs e)
        {
            TraerMaestro("");
        }

   }
}
