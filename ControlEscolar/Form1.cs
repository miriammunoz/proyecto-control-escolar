﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class frmUsuarios : Form
    {
        private UsuarioManejador _UsuarioManejador;
        private Usuario _usuario; 
        public frmUsuarios()
        {
            InitializeComponent();
            _UsuarioManejador = new UsuarioManejador();
            _usuario = new Usuario();
        }
        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            BuscarUsuarios("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtNombre.Focus();
        }
        private void BuscarUsuarios(string filtro)
        {
            dgvUsuarios.DataSource = _UsuarioManejador.GetUsuarios(filtro);
        }
        //CARGAR USUARIO
        private void CagarUsuario()
        {
            _usuario.IdUsuario = Convert.ToInt32(lblId.Text);
            _usuario.Nombre = txtNombre.Text;
            _usuario.ApellidoPaterno = txtAP.Text;
            _usuario.ApellidoMaterno = txtAM.Text;
            _usuario.Contraseña = txtContraseña.Text;
        }
        private void GuardarUsuario()
        {
            _UsuarioManejador.Guardar(_usuario);
        }
        //VALIDACION
        private bool ValidarUsuario()
        {
            var tupla = _UsuarioManejador.ValidarUsuario(_usuario);
            var valido = tupla.Item1;
            var mensaje = tupla.Item2;

            if (!valido)
            {
                MessageBox.Show(mensaje, "Error de validación", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return valido;
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                CagarUsuario();

                if (ValidarUsuario())
                {
                    ControlarBotones(true, false, false, true);
                    ControlarCuadros(false);
                    GuardarUsuario();
                    LimpiarCuadros();
                    BuscarUsuarios("");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtAP.Enabled = activar;
            txtAM.Enabled = activar;
            txtContraseña.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtAP.Text = "";
            txtAM.Text = "";
            txtContraseña.Text = "";
            lblId.Text = "0";
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarUsuarios(txtBuscar.Text);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estas seguro que deseas eliminar este registro?","Eliminar Registro",MessageBoxButtons.YesNo)==DialogResult.Yes)
            {
                try
                {
                    EliminarUsuario();
                    BuscarUsuarios("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
            
        }
        private void EliminarUsuario()
        {
            var IdUsuario = dgvUsuarios.CurrentRow.Cells["idUsuario"].Value;
            _UsuarioManejador.Eliminar(Convert.ToInt32(IdUsuario));
        }

        private void dgvUsuarios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarUsuario();
                BuscarUsuarios("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void ModificarUsuario()
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
        
            lblId.Text= dgvUsuarios.CurrentRow.Cells["Idusuario"].Value.ToString();
            txtNombre.Text = dgvUsuarios.CurrentRow.Cells["Nombre"].Value.ToString();
            txtAP.Text = dgvUsuarios.CurrentRow.Cells["ApellidoMaterno"].Value.ToString();
            txtAM.Text = dgvUsuarios.CurrentRow.Cells["ApellidoPaterno"].Value.ToString();
            txtContraseña.Text = dgvUsuarios.CurrentRow.Cells["Contraseña"].Value.ToString();

        }

        private void dgvUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
