﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;


namespace ControlEscolar
{
    public partial class Maestros : Form
    {
        private MaestrosManejador _MaestroManejador;
        private EstadosManejador _EstadosManejador;
        private CiudadesManejador _CiudadesManejador;
        private EstudiosManejador _EstudiosManejador;
      

        Form frmE = new frmEstudios();

        public Maestros()
        {
            InitializeComponent();
            _MaestroManejador = new MaestrosManejador();
            _EstadosManejador = new EstadosManejador();
            _CiudadesManejador = new CiudadesManejador();
            _EstudiosManejador = new EstudiosManejador();
 
        }  
        private void BuscarMaestro(string filtro)
        {
            dgvMaestros.DataSource = _MaestroManejador.GetMaestros(filtro);
        }
        private void TraerEstados(string filtro)
        {
            cbxEstados.DataSource = _EstadosManejador.GetEstados(filtro);
            cbxEstados.DisplayMember = "Nombre";
            cbxEstados.ValueMember = "Codigo";
        }
        private void TraerCiudades(string filtro)
        {
            cbxCiudades.DataSource = _CiudadesManejador.GetCiudades(cbxEstados.SelectedValue.ToString());
            cbxCiudades.DisplayMember = "NombreCiudad";
            cbxCiudades.ValueMember = "CodigoCiudad";
        }
        
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtAP.Enabled = activar;
            txtAM.Enabled = activar;
            txtFecha.Enabled = activar;
            txtDomicilio.Enabled = activar;
            cbxEstados.Enabled = activar;
            cbxCiudades.Enabled = activar;

 
        }
        private void LimpiarCuadros()
        {
            lblNumeroControl.Text = "0";
            txtNombre.Text = "";
            txtAP.Text = "";
            txtAM.Text = "";
            txtFecha.Text = "";
            txtDomicilio.Text = "";
            lblId.Text = "0";
            cbxEstados.Text = "";
            cbxCiudades.Text = "";
           
        }

        private void GuardarMaestro()
        {
            _MaestroManejador.Guardar(new Entidades.ControlEscolar.Maestros
            {
                Idmaestro = Convert.ToInt32(lblId.Text),
                Numerocontrol = lblNumeroControl.Text,
                Nombre = txtNombre.Text,
                ApellidoPaterno = txtAP.Text,
                ApellidoMaterno = txtAM.Text,
                FechaNacimiento = txtFecha.Text,
                Direccion = txtDomicilio.Text,
                Estado = cbxEstados.SelectedValue.ToString(),
                Ciudad = cbxCiudades.SelectedValue.ToString(),
             
            });
        }
        private void EliminarMaestro()
        {
            var Id = dgvMaestros.CurrentRow.Cells["Id"].Value;
            _MaestroManejador.Eliminar(Convert.ToInt32(Id));
        }
 
        private void ModificarMaestro()
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);

            lblId.Text = dgvMaestros.CurrentRow.Cells["idmaestro"].Value.ToString();
            lblNumeroControl.Text = dgvMaestros.CurrentRow.Cells["NumeroControl"].Value.ToString();
            txtNombre.Text = dgvMaestros.CurrentRow.Cells["Nombre"].Value.ToString();
            txtAP.Text = dgvMaestros.CurrentRow.Cells["ApellidoPaterno"].Value.ToString();
            txtAM.Text = dgvMaestros.CurrentRow.Cells["ApellidoMaterno"].Value.ToString();
            txtFecha.Text = dgvMaestros.CurrentRow.Cells["FechaNacimiento"].Value.ToString();
            txtDomicilio.Text = dgvMaestros.CurrentRow.Cells["Direccion"].Value.ToString();
            cbxEstados.Text = dgvMaestros.CurrentRow.Cells["fkCodigo"].Value.ToString();
            cbxCiudades.Text = dgvMaestros.CurrentRow.Cells["FkCodigoCiudad"].Value.ToString();

        }

        private void dgvMaestros_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarMaestro();
                BuscarMaestro("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void cbxEstados_SelectedIndexChanged(object sender, EventArgs e)
        {
            TraerCiudades("");
        }

        private void cbxEstados_Click(object sender, EventArgs e)
        {
            TraerEstados("");
        }

        private void cbxCedula_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void Maestros_Load(object sender, EventArgs e)
        {
            BuscarMaestro("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            TraerEstados("");
            TraerCiudades("");

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            NumControl();
           
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);

            try
            {
                GuardarMaestro();
                LimpiarCuadros();
                BuscarMaestro("");
                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("¿Estas seguro que deseas eliminar este registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarMaestro();
                    BuscarMaestro("");
                    
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        /*private void txtNumeroControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }          
            else
            {
                e.Handled = true;
                MessageBox.Show("Solo se permiten Numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        */

    
        private void btnChecar_Click(object sender, EventArgs e)
        {
            frmE.Show();         
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarMaestro(txtBuscar.Text);
        }
        private void NumControl()
        {
           // int Fecha = Convert.ToInt32(cbxAño.SelectedValue);
            DateTime d = DateTime.Now;
            int anio = d.Year;
            int id = 0;
            DataSet ds;
            ds = _MaestroManejador.NumeroControl(anio.ToString());
            try
            {
                string a = ds.Tables[0].Rows[0]["NumeroControl"].ToString();
                int i = Convert.ToInt32(a) + 1;
                if (i>=10)
                {
                    lblNumeroControl.Text = "D" + anio.ToString() + i;
                }
                else
                {
                    lblNumeroControl.Text = "D" + anio.ToString() +"0"+ i;
                }
            }
            catch (Exception)
            {
                id++;
                lblNumeroControl.Text = "D" + anio.ToString() + "0" + id.ToString();
            }

        }
    }
}
