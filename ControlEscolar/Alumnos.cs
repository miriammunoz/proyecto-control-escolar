﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class frmAlumnos : Form
    {
        private AlumnoManejador _alumnoManejador;
        private EstadosManejador _EstadosManejador;
        private CiudadesManejador _CiudadesManejador;
        public frmAlumnos()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _EstadosManejador = new EstadosManejador();
            _CiudadesManejador = new CiudadesManejador();
        }
        private void BuscarAlumno(string filtro)
        {
            dgvAlumnos.DataSource = _alumnoManejador.GetAlumnos(filtro);
        }
        private void TraerEstados(string filtro)
        {
            cbxEstados.DataSource = _EstadosManejador.GetEstados(filtro);
            cbxEstados.DisplayMember = "Nombre";
            cbxEstados.ValueMember = "Codigo";      
        }
        private void TraerCiudades(string filtro)
        {
            cbxCiudades.DataSource = _CiudadesManejador.GetCiudades(cbxEstados.SelectedValue.ToString());
            cbxCiudades.DisplayMember = "NombreCiudad";
            cbxCiudades.ValueMember = "CodigoCiudad";
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtNumeroControl.Enabled = activar;
            txtNombre.Enabled = activar;
            txtAP.Enabled = activar;
            txtAM.Enabled = activar;
            txtFecha.Enabled = activar;
            txtDomicilio.Enabled = activar;
            txtEmail.Enabled = activar;
            txtSexo.Enabled = activar;
            cbxEstados.Enabled = activar;
            cbxCiudades.Enabled = activar;
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtNombre.Focus();
        }
        private void LimpiarCuadros()
        {
            txtNumeroControl.Text = "";
            txtNombre.Text = "";
            txtAP.Text = "";
            txtAM.Text = "";
            txtFecha.Text = "";
            txtDomicilio.Text = "";
            txtSexo.Text = "";
            txtEmail.Text = "";
            lblId.Text = "0";
            cbxEstados.Text = "";
            cbxCiudades.Text = "";
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);

            try
            {
                GuardarAlumno();
                LimpiarCuadros();
                BuscarAlumno("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void GuardarAlumno()
        {
            _alumnoManejador.Guardar(new Alumnos
            {
                Id= Convert.ToInt32(lblId.Text),
                NumeroControl=Convert.ToInt32(txtNumeroControl.Text),
                Nombre = txtNombre.Text,
                ApellidoPaterno = txtAP.Text,
                ApellidoMaterno = txtAM.Text,
                FechaNacimiento = txtFecha.Text,
                Sexo=txtSexo.Text,
                Domicilio=txtDomicilio.Text,
                Email=txtEmail.Text,
                FkCodigo=cbxEstados.SelectedValue.ToString(),
                FkCodigoCiudad=Convert.ToInt32(cbxCiudades.SelectedValue)
            });
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("¿Estas seguro que deseas eliminar este registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarAlumno();
                    BuscarAlumno("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void EliminarAlumno()
        {
            var Id = dgvAlumnos.CurrentRow.Cells["Id"].Value;
            _alumnoManejador.Eliminar(Convert.ToInt32(Id));
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            BuscarAlumno(txtBuscar.Text);
        }
        private void dgvAlumnos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarAlumno();
                BuscarAlumno("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void ModificarAlumno()
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);

            var alumno =_alumnoManejador.GetAlumnobyId(Convert.ToInt32(dgvAlumnos.CurrentRow.Cells["Id"].Value));

            lblId.Text = alumno.Id.ToString();
            txtNumeroControl.Text = alumno.NumeroControl.ToString();
            txtNombre.Text = alumno.Nombre.ToString();
            txtAP.Text = alumno.ApellidoPaterno.ToString();
            txtAM.Text =alumno.ApellidoMaterno.ToString();
            txtFecha.Text =alumno.FechaNacimiento.ToString();
            txtDomicilio.Text = alumno.Domicilio.ToString();
            txtSexo.Text = alumno.Sexo.ToString();
            txtEmail.Text = alumno.Email.ToString();
            cbxEstados.Text =alumno.FkCodigo.ToString();
            cbxCiudades.Text = alumno.FkCodigoCiudad.ToString();
        }
        private void cbxEstados_Click(object sender, EventArgs e)
        {
            TraerEstados("");
        }
        private void cbxEstados_SelectedIndexChanged(object sender, EventArgs e)
        {
            TraerCiudades("");
        }
        private void frmAlumnos_Load(object sender, EventArgs e)
        {
            BuscarAlumno("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            TraerEstados("");
            TraerCiudades("");

        }
    }
}
