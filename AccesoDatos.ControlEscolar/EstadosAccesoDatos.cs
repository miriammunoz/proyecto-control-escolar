﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class EstadosAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public EstadosAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Empresa", 3309);
        }
        public List<Estados> GetEstados(string filtro)
        {
            var listEstados = new List<Estados>();

            var ds = new DataSet();
            string consulta = "select * from Estados where Nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "Estados");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var estados = new Estados
                {
                    Codigo = row["Codigo"].ToString(),
                    Nombre = row["Nombre"].ToString(),

                };
                listEstados.Add(estados);
            }

            return listEstados;

        }
    }
}
