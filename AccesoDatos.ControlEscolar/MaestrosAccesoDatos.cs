﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class MaestrosAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public MaestrosAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Empresa", 3309);
        }
        public void Guardar(Maestros maestros)
        {
            if (maestros.Idmaestro == 0)
            {
                string consulta = string.Format("insert into Maestros values" +
                    "(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')",
                    maestros.Numerocontrol, maestros.Nombre, maestros.ApellidoPaterno, maestros.ApellidoMaterno,
                    maestros.FechaNacimiento, maestros.Direccion, maestros.Estado,
                    maestros.Ciudad);

                conexion.EjecutarConsulta(consulta);
            }
            else   //Update
            {
                string consulta = string.Format("update Maestros set NumeroControl='{0}',Nombre='{1}'," +
                    " ApellidoPaterno='{2}', ApellidoMaterno='{3}', FechaNacimiento='{4}', Direccion='{5}'," +
                    " fkCodigo='{6}', fkCodigoCiudad='{7}' where idmaestro={8}",
                    maestros.Numerocontrol, maestros.Nombre, maestros.ApellidoPaterno, maestros.ApellidoMaterno,
                    maestros.FechaNacimiento, maestros.Direccion, maestros.Estado,
                    maestros.Ciudad, maestros.Idmaestro);

                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int Id)
        {
            string consulta = string.Format("delete from Maestros where idmaestro={0}", Id);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Maestros> GetMaestros(string filtro)
        {
            var listMaestros = new List<Maestros>();

            var ds = new DataSet();
           //string consulta = "select * from Maestros where Nombre like '%" + filtro + "%'";
           string consulta = "select idmaestro as 'Id', NumeroControl, Maestros.Nombre, ApellidoPaterno, ApellidoMaterno, FechaNacimiento, Direccion, Estados.Nombre AS 'Estado', NombreCiudad AS 'Municipio' from Maestros, Estados, Ciudades where Maestros.fkCodigoCiudad = Ciudades.CodigoCiudad and Maestros.fkCodigo = Estados.Codigo;";
            ds = conexion.ObtenerDatos(consulta, "Maestros");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var maestros = new Maestros
                {
                    Idmaestro = Convert.ToInt32(row["Id"]),
                    Numerocontrol = row["NumeroControl"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    ApellidoPaterno = row["ApellidoPaterno"].ToString(),
                    ApellidoMaterno = row["ApellidoMaterno"].ToString(),
                    FechaNacimiento = row["FechaNacimiento"].ToString(),
                    Direccion = row["Direccion"].ToString(),
                    Estado = row["Estado"].ToString(),
                    Ciudad = row["Municipio"].ToString()
                };
                listMaestros.Add(maestros);
            }
            return listMaestros;
        }
               
        public DataSet NumeroControl(string i)
        {
            var ds = new DataSet();
            string consulta = string.Format("select max(substring(numerocontrol,-2)) as 'numerocontrol' from Maestros where numerocontrol like '%" + i + "%'");
            ds = conexion.ObtenerDatos(consulta, "Maestros");
            return ds;
        }
    }
}
